/*
 * FreeRTOS Kernel V10.2.0
 * Copyright (C) 2019 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

#include "device.h"

/* RTOS includes. */
#include "FreeRTOS.h"
#include "task.h"

#define UNUSED(x) (void)(x)

/* Task priorities. */
#define LED1_TASK_PRIORITY    (tskIDLE_PRIORITY + 2)
#define LED1_TASK_STACK_SIZE  64

portTASK_FUNCTION_PROTO(Led1TaskFunction, pams);

portTASK_FUNCTION(Led1TaskFunction, pams)
{
  UNUSED(pams);  // Avoid compiler unused parameter warnings

  const TickType_t led1_semiperiod = 500 / portTICK_PERIOD_MS;
  int state = 0;
  for( ;; )
  {
    Pin_LED1_Write(state);
    vTaskDelay(led1_semiperiod);
    state = state ? 0 : 1;
  }
}

/*---------------------------------------------------------------------------*/
/*
 * Installs the RTOS interrupt handlers.
 */

static void FreeRTOS_Start( void );

void FreeRTOS_Start( void )
{
  /* Port layer functions that need to be copied into the vector table. */
  extern void xPortPendSVHandler( void );
  extern void xPortSysTickHandler( void );
  extern void vPortSVCHandler( void );
  extern cyisraddress CyRamVectors[];

	/* Install the OS Interrupt Handlers. */
	CyRamVectors[ 11 ] = (cyisraddress) vPortSVCHandler;
	CyRamVectors[ 14 ] = (cyisraddress) xPortPendSVHandler;
	CyRamVectors[ 15 ] = (cyisraddress) xPortSysTickHandler;

}

/*---------------------------------------------------------------------------*/

int main( void )
{
  /* Place your initialization/startup code here (e.g. MyInst_Start()) */
	FreeRTOS_Start();  // Start FreeRTOS
	UART_1_Start(); 	 // Start UART_1

	/* Start demo tasks.  These are just here to exercise the
	kernel port and provide examples of how the FreeRTOS API can be used. */
  TaskHandle_t led1_task = NULL;
  xTaskCreate(Led1TaskFunction, "LED1 task", LED1_TASK_STACK_SIZE, NULL, LED1_TASK_PRIORITY, &led1_task);

	/* Will only get here if there was insufficient memory to create the idle
    task.  The idle task is created within vTaskStartScheduler(). */
	vTaskStartScheduler();

	/* Should never reach here as the kernel will now be running.  If
	vTaskStartScheduler() does return then it is very likely that there was
	insufficient (FreeRTOS) heap space available to create all the tasks,
	including the idle task that is created within vTaskStartScheduler() itself. */
	for( ;; )
  {
  }
  
  return 0;
}

/*---------------------------------------------------------------------------*/

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	/* The stack space has been execeeded for a task, considering allocating more. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}

/*---------------------------------------------------------------------------*/

void vApplicationMallocFailedHook( void )
{
	/* The heap space has been execeeded. */
	taskDISABLE_INTERRUPTS();
	for( ;; );
}

/*---------------------------------------------------------------------------*/
